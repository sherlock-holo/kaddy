package kaddy.kio

import kotlinx.coroutines.experimental.nio.aConnect
import kotlinx.coroutines.experimental.runBlocking
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousSocketChannel

fun main(args: Array<String>) = runBlocking {
    val client = AsynchronousSocketChannel.open()
    client.aConnect(InetSocketAddress("127.0.0.1", 9876))

    val readBuffer = ReadBuffer(client)

    println(readBuffer.readLine())
}
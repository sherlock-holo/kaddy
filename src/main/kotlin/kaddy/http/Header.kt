package kaddy.http

class Header {
    var version = Version.HTTP_1_1
    val map = HashMap<String, String>()
}
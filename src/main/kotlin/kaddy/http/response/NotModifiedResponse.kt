package kaddy.http.response

import kaddy.http.ResponseStatus
import kaddy.kio.WriteBuffer
import java.nio.channels.AsynchronousSocketChannel

class NotModifiedResponse(conn: AsynchronousSocketChannel, etag: String) : Response() {
    override val responseStatus = ResponseStatus.NOT_MODIFIED

    private val writeBuffer = WriteBuffer(conn, 500)

    init {
        header.map["Etag"] = etag
    }

    override suspend fun write() {
        writeBuffer.write(headerToByteArray())
    }
}
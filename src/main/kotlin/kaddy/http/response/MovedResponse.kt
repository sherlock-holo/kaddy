package kaddy.http.response

import kaddy.http.ResponseStatus
import kaddy.kio.WriteBuffer
import java.io.IOException
import java.nio.channels.AsynchronousSocketChannel

class MovedResponse(
        conn: AsynchronousSocketChannel,
        override val responseStatus: ResponseStatus,
        private val newUrl: String
) : Response() {

    private val writeBuffer = WriteBuffer(conn, 500)

    init {
        header.map["Content-Type"] = "text/html; charset=utf-8"
    }

    override suspend fun write() {
        header.map["Location"] = newUrl

        val info = when (responseStatus) {
            ResponseStatus.MOVED_PERMANENTLY -> {
                "<a href=\"$newUrl/\">Moved Permanently</a>.".toByteArray()
            }

            ResponseStatus.FOUND -> {
                "<a href=\"$newUrl/\">Moved Temporarily</a>.".toByteArray()
            }

            else -> throw IllegalStateException("MovedResponse just can be used to redirect")
        }

        contentLength = info.size
        try {
            writeBuffer.write(headerToByteArray() + info)
        } catch (e: IOException) {
            writeBuffer.close()
        }
    }
}
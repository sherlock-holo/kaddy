package kaddy.config

import java.io.File
import java.lang.IllegalArgumentException
import java.net.InetSocketAddress
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class Config {
    private val unitsMap = HashMap<Int, ArrayList<ConfigUnit>>()
    val portSet = HashSet<Int>()

    fun getConfigUnit(host: String, port: Int): ConfigUnit? {
        val units = unitsMap[port] ?: return null

        var realHost = host

        if (port != 80 && port != 443) realHost += ":$port"

        for (unit in units) {
            if (unit.host == realHost) return unit
        }

        return null
    }


    fun getConfigUnits(port: Int) = unitsMap[port]

    companion object {
        fun parseConfigFile(file: File): Config {
            if (!file.exists()) throw NoSuchFileException(file)

            val config = Config()
            val bufferedReader = file.bufferedReader()

            var unit: ConfigUnit? = null
            var start = false

            loop@ while (true) {
                var line = bufferedReader.readLine()?.replace(Regex("[' ]+"), " ") ?: break@loop

                when {
                    line == "" -> continue@loop

                    line.startsWith('#') || line.startsWith(" #") -> continue@loop

                    line.contains('{') -> {
                        if (start) throw IllegalArgumentException("config syntax error")

                        start = true
                        unit = ConfigUnit()
                    }

                    line.contains('}') -> {
                        if (!start) throw IllegalArgumentException("config syntax error")

                        var units = config.unitsMap[unit!!.port]
                        if (units == null) {
                            config.unitsMap[unit.port] = ArrayList()
                            units = config.unitsMap[unit.port]!!
                        }

                        if (!unit.checkConfig()) throw IllegalArgumentException("config syntax error")

                        units.add(unit)
                        config.portSet.add(unit.port)
                        start = false
                    }

                    else -> {
                        unit ?: throw IllegalArgumentException("config syntax error")

                        val firstNotSpaceIndex = line.indexOfFirst { it != ' ' }
                        val lastNotSpaceIndex = line.indexOfLast { it != ' ' }
                        line = line.substring(firstNotSpaceIndex..lastNotSpaceIndex)

                        val list = line.split(' ')

                        when (list[0]) {
                            "host" -> unit.host = list[1]

                            "port" -> unit.port = list[1].toInt()

                            "root" -> unit.root = list[1]

                            "index" -> unit.index = list[1]

                            "redirect" -> {
                                when (list[1].toInt()) {
                                    301 -> unit.status = ConfigUnitStatus.REDIRECT_301
                                    302 -> unit.status = ConfigUnitStatus.REDIRECT_302

                                    else -> throw IllegalArgumentException("config syntax error")
                                }

                                unit.redirectPath = list[2]
                                unit.redirectTarget = list[3]
                            }

                            "proxy" -> {
                                unit.status = ConfigUnitStatus.PROXY

                                when (list.size) {
                                    2 -> {
                                        unit.proxyPath = "/"
                                        unit.proxyTarget = list[1]
                                    }

                                    3 -> {
                                        unit.proxyPath = list[1]
                                        unit.proxyTarget = list[2]
                                    }

                                    else -> throw IllegalArgumentException("config syntax error")
                                }

                                if (unit.proxyTarget.startsWith("https://")) {
                                    unit.proxyTLS = true
                                    unit.proxyTarget.removePrefix("https://").apply {
                                        unit.proxyHost = this
                                    }
                                } else {
                                    unit.proxyTarget.removePrefix("http://").apply { unit.proxyHost = this }
                                }

                                if (".*:[0-9]+".toRegex().matches(unit.proxyHost)) unit.proxyHost.split(':').apply {
                                    unit.proxyAddress = InetSocketAddress(this[0], this[1].toInt())
                                } else {
                                    if (unit.proxyTLS) unit.proxyAddress = InetSocketAddress(unit.proxyHost, 443)
                                    else unit.proxyAddress = InetSocketAddress(unit.proxyHost, 80)
                                }
                            }

                            else -> throw IllegalArgumentException("config syntax error")
                        }
                    }
                }
            }

            if (start) throw IllegalArgumentException("config syntax error")

            bufferedReader.close()
            return config
        }

        class ConfigUnit {
            var host = ""
                get() {
                    return if (port != 80 || port != 443) "$field:$port"
                    else field
                }

            var port = 80

            lateinit var root: String

            var status = ConfigUnitStatus.OK

            var index = "index.html"


            lateinit var redirectPath: String

            lateinit var redirectTarget: String


            lateinit var proxyPath: String
            lateinit var proxyTarget: String
            lateinit var proxyHost: String
            var proxyTLS = false

            lateinit var proxyAddress: InetSocketAddress

            fun checkConfig(): Boolean {
                if (host == "" || host == ":$port") return false

                when (status) {
                    ConfigUnitStatus.OK -> {
                        if (!this::root.isInitialized) return false
                    }

                    ConfigUnitStatus.REDIRECT_301, ConfigUnitStatus.REDIRECT_302 -> {
                        if (!this::redirectPath.isInitialized || !this::redirectTarget.isInitialized) return false
                    }

                    ConfigUnitStatus.PROXY -> {
                        if (!this::proxyPath.isInitialized || !this::proxyTarget.isInitialized
                                || !this::proxyHost.isInitialized || !this::proxyAddress.isInitialized) return false
                    }
                }

                return true
            }
        }
    }
}
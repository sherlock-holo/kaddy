package kaddy.http

import kaddy.kio.ReadBuffer
import java.io.IOException
import java.net.InetSocketAddress

class Request {
    lateinit var header: Header

    val host get() = header.map["Host"]!!
    val version get() = header.version

    lateinit var method: Method
        private set

    lateinit var uri: String
        private set

    lateinit var path: String
        private set

    var query: String? = null
        private set

    var content: ByteArray? = null

    lateinit var remoteAddress: InetSocketAddress
        private set

    val port get() = remoteAddress.port

    var gzip = false
        private set

    val ifNoneMatch: String?
        get() {
            return header.map["If-None-Match"]
        }

    /** only support GET request for the time **/
    fun toByteArray(): ByteArray {
        val arrayList = ArrayList<Byte>(500)

        val sb = StringBuilder()

        sb.append(method.string)
        sb.append(' ')
        sb.append(uri)
        sb.append(' ')
        sb.append(version.string)
        sb.append("\r\n")
        arrayList.addAll(sb.toString().toByteArray().toTypedArray())
        sb.delete(0, sb.length)

        header.map.forEach { key, value ->
            sb.append(key)
            sb.append(": ")
            sb.append(value)
            sb.append("\r\n")
            arrayList.addAll(sb.toString().toByteArray().toTypedArray())
            sb.delete(0, sb.length)
        }

        arrayList.addAll("\r\n".toByteArray().toTypedArray())

        return arrayList.toByteArray()
    }

    companion object {
        suspend fun buildRequest(readBuffer: ReadBuffer): Request {
            readBuffer.separator = "\r\n"

            val request = Request()
            val header = Header()

            val rawMethod = readBuffer.readLine()?.split(' ') ?: throw IOException("unexpected end of stream")

            when (rawMethod[0]) {
                "GET" -> {
                    request.method = Method.GET
                }

                "POST" -> {
                    request.method = Method.POST
                }
            }

            request.uri = rawMethod[1]

            if (request.uri.contains('?')) {
                val list = request.uri.split('?')
                request.path = list[0]
                request.query = list[1]
            } else {
                request.path = request.uri
            }

            when (rawMethod[2]) {
                "HTTP/1.1" -> {
                    header.version = Version.HTTP_1_1
                }
            }

            while (true) {
                val string = readBuffer.readLine() ?: throw IOException("unexpected end of stream")

                if (string == "") break

                val key = string.substring(0 until string.indexOf(": "))
                val value = string.substring(key.length + 2..string.lastIndex)
                header.map[key] = value
            }

            request.header = header

            request.remoteAddress = (readBuffer.channel.remoteAddress as InetSocketAddress)

            if (header.map["Accept-Encoding"]?.contains("gzip") == true) request.gzip = true

            return when (request.method) {
                Method.GET -> request

                Method.POST -> {
                    val contentLength = header.map["Content-Length"]?.toInt()
                            ?: throw IllegalStateException("can't find Content-Length")

                    val content = ByteArray(contentLength)
                    if (readBuffer.read(content) == 0) throw IOException("unexpected end of stream")

                    request.content = content

                    request
                }
            }.apply {
                readBuffer.clear()
            }
        }
    }
}
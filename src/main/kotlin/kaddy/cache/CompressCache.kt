package kaddy.cache

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.newFixedThreadPoolContext
import java.io.File
import java.io.IOException
import java.util.zip.GZIPOutputStream

class CompressCache(
        root: String = System.getProperty("java.io.tmpdir"),
        path: String = "kaddy-compress-cache",
        threads: Int = Runtime.getRuntime().availableProcessors()
) {

    private val threadsPool = newFixedThreadPoolContext(threads, "compress")
    private val directory = File(root, path)
    private val fileMap = HashMap<String, File>()

    init {
        if (directory.exists()) {
            clean(directory)
        }
        directory.mkdir()
    }

    private fun clean(file: File) {
        if (file.isDirectory) {
            val list = file.list()
            list.forEach {
                clean(File(file.absolutePath, it))
            }
            file.delete()
        } else file.delete()
    }

    suspend fun getCache(originFile: File): File {
        return if (originFile.absolutePath in fileMap) fileMap[originFile.absolutePath]!!
        else {
            val cacheFile = compress(originFile)

            fileMap[originFile.absolutePath] = cacheFile
            cacheFile
        }
    }

    private suspend fun compress(originFile: File): File {
        val cacheFile = File(directory, "kaddy-${originFile.hashCode()}")

        if (cacheFile.exists()) cacheFile.delete()

        async(threadsPool) {
            try {
                val fileOutputStream = cacheFile.outputStream()
                val gzip = GZIPOutputStream(fileOutputStream)
                gzip.write(originFile.readBytes())
                gzip.finish()
                gzip.flush()
                gzip.close()
                fileOutputStream.flush()
                fileOutputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }.await()

        return cacheFile
    }

    suspend fun updateCache(originFile: File): Boolean {
        val oldCacheFile = fileMap[originFile.absolutePath] ?: return false

        oldCacheFile.delete()

        return try {
            fileMap[originFile.absolutePath] = compress(originFile)
            true
        } catch (e: IOException) {
            fileMap.remove(originFile.absolutePath)
            false
        }
    }

    companion object {
        val compressData = arrayOf(
                "text/plain",
                "text/html",
                "text/css",
                "application/json",
                "application/javascript"
        )
    }
}
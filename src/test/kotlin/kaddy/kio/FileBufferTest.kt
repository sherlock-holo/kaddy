package kaddy.kio

import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.File

class FileBufferTest {

    @Test
    fun readLine() {
        runBlocking {
            val fileBuffer = FileBuffer("/tmp/test.txt")
            assertEquals("sherlockholo", fileBuffer.readLine())
        }
    }

    @Test
    fun readBytes() {
        runBlocking {
            val fileBuffer = FileBuffer("/tmp/test.txt")
            assertArrayEquals(File("/tmp/test.txt").readBytes(), fileBuffer.readBytes())
        }
    }
}
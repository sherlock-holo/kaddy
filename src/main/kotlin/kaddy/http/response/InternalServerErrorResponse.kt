package kaddy.http.response

import kaddy.http.ResponseStatus
import kaddy.kio.WriteBuffer
import java.nio.channels.AsynchronousSocketChannel

class InternalServerErrorResponse(conn: AsynchronousSocketChannel, private val errorMessage: String = "Server error") : Response() {
    override val responseStatus = ResponseStatus.INTERNAL_SERVER_ERROR

    private val writeBuffer = WriteBuffer(conn, 500)

    init {
        header.map["Connection"] = "close"
    }

    override suspend fun write() {
        contentLength = errorMessage.length
        writeBuffer.write(headerToByteArray())
        writeBuffer.write(errorMessage.toByteArray())
    }
}
package kaddy.kio

import kotlinx.coroutines.experimental.nio.aRead
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousSocketChannel

class ReadBuffer(
        val channel: AsynchronousSocketChannel,
        capacity: Int = 81920,
        directByteBuffer: Boolean = false
) : Buffer() {

    var separator: String = "\n"

    val remaining get() = innerBuffer.capacity() - innerBuffer.position()

    private val innerBuffer =
            if (!directByteBuffer) {
                ByteBuffer.allocate(capacity)
            } else ByteBuffer.allocateDirect(capacity)

    private var bufferContentLength = 0

    private val sb = StringBuilder()

    private var readFin = false

    override suspend fun read(dst: ByteArray): Int {
        if (readFin) throw IOException("end of stream")
        if (dst.size > innerBuffer.capacity() - innerBuffer.position()) {
            throw IndexOutOfBoundsException("dst size is larger than readable data size")
        }

        while (bufferContentLength < dst.size) {
            val readLength = channel.aRead(innerBuffer)
            if (readLength > 0) {
                bufferContentLength += readLength

            } else {
                readFin = true
                break
            }
        }

        innerBuffer.flip()

        var length = innerBuffer.remaining()

        if (length > dst.size) length = dst.size

        innerBuffer.get(dst, 0, length)

        innerBuffer.compact()

        bufferContentLength -= length

        return length
    }

    suspend fun read(): Byte? {
        val byteArray = ByteArray(1)
        read(byteArray).let {
            return if (it == 0) null
            else byteArray[0]
        }
    }

    suspend fun readInt(): Int? {
        val byteArray = ByteArray(4)
        read(byteArray).let {
            return if (it == 0) null
            else {
                ByteBuffer.wrap(byteArray).int
            }
        }
    }

    suspend fun readShort(): Short? {
        val byteArray = ByteArray(2)
        read(byteArray).let {
            return if (it == 0) null
            else {
                ByteBuffer.wrap(byteArray).short
            }
        }
    }

    suspend fun readLong(): Long? {
        val byteArray = ByteArray(8)
        read(byteArray).let {
            return if (it == 0) null
            else {
                ByteBuffer.wrap(byteArray).long
            }
        }
    }

    override suspend fun readLine(): String? {
        sb.delete(0, sb.length)

        val byteArray = ByteArray(1)

        while (true) {
            if (read(byteArray) == 0) {
                return null
            } else {
                val char = byteArray[0].toChar()

                sb.append(char)
                if (sb.endsWith(separator)) return sb.apply { this.delete(lastIndexOf(separator), this.length) }.toString()
            }
        }
    }

    override suspend fun write(byteArray: ByteArray, offset: Int, length: Int): Int {
        throw IllegalAccessException("ReadBuffer can't write")
    }

    override suspend fun writeLine(string: String): Int {
        throw IllegalAccessException("ReadBuffer can't write")
    }

    override fun clear() {
        bufferContentLength = 0
        innerBuffer.clear()
    }

    override fun close() {
        channel.close()
    }
}
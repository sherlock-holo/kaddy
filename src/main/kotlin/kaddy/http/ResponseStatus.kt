package kaddy.http

enum class ResponseStatus(val codeAndStatus: String) {
    OK("200 OK"),
    NOT_FOUND("404 Not Found"),
    MOVED_PERMANENTLY("301 Moved Permanently"),
    FOUND("302 Moved Temporarily"),
    NOT_MODIFIED("304 Not Modified"),
    INTERNAL_SERVER_ERROR("500 Internal Server Error")
}
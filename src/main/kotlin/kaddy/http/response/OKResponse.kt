package kaddy.http.response

import kaddy.cache.CompressCache
import kaddy.cache.EtagCache
import kaddy.http.ResponseStatus
import kaddy.kio.FileBuffer
import kaddy.kio.WriteBuffer
import java.io.File
import java.io.IOException
import java.nio.channels.AsynchronousSocketChannel
import java.nio.file.Files
import java.nio.file.Paths

class OKResponse(
        conn: AsynchronousSocketChannel,
        private val html: File,
        private val compressCache: CompressCache? = null,
        private val ifNoneMatch: String? = null
) : Response() {

    override val responseStatus = ResponseStatus.OK
    private val writeBuffer = WriteBuffer(conn)

    private val contentType: String

    init {
        contentType = Files.probeContentType(Paths.get(html.toURI())).let {
            if (it == null) return@let "application/octet-stream"

            if (it.endsWith("html")) return@let "$it; charset=utf-8"
            else return@let it
        }

        header.map["Content-Type"] = contentType
    }

    override suspend fun write() {
        val fileBuffer =
                if (compressCache == null || html.length() < 1024 || CompressCache.compressData.none { contentType.contains(it) }) FileBuffer(html)
                else {
                    header.map["Content-Encoding"] = "gzip"

                    val etag = html.lastModified().toString()
                    header.map["Etag"] = etag

                    when (EtagCache.getCache(ifNoneMatch)) {

                        EtagCache.EtagStatus.EMPTY, EtagCache.EtagStatus.NEW -> {
                            EtagCache.addCache(etag, html.absolutePath)
                            FileBuffer(compressCache.getCache(html))
                        }

                        EtagCache.EtagStatus.OLD -> {
                            EtagCache.updateCache(ifNoneMatch!!, etag, html.absolutePath)

                            val updateResult = compressCache.updateCache(html)

                            if (!updateResult) {
                                FileBuffer(html)
                            } else FileBuffer(compressCache.getCache(html))
                        }
                    }

                }

        contentLength = fileBuffer.size.toInt()

        writeBuffer.write(headerToByteArray())

        val byteArray = ByteArray(79 * 1024)

        try {
            while (true) {
                val length = fileBuffer.read(byteArray)
                writeBuffer.write(byteArray, 0, length)

                if (length < byteArray.size) break
            }
        } catch (e: IOException) {
            writeBuffer.close()
        } finally {
            fileBuffer.close()
        }
    }
}
package kaddy.server

import kaddy.cache.CompressCache
import kaddy.config.Config
import kotlinx.coroutines.experimental.runBlocking
import java.io.File

fun main(args: Array<String>) = runBlocking {
    val config = Config.parseConfigFile(File("/tmp/config.conf"))

    val serverUnit = ServerUnit(8000, config.getConfigUnits(8000) ?: throw Throwable(), CompressCache())
    serverUnit.start()
}
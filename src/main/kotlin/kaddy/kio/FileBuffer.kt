package kaddy.kio

import kotlinx.coroutines.experimental.nio.aRead
import kotlinx.coroutines.experimental.nio.aWrite
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousFileChannel
import java.nio.file.Paths

class FileBuffer(file: File, capacity: Int = 81920) : Buffer() {
    constructor(filePath: String, capacity: Int = 81920) : this(File(filePath), capacity)

    var separator: String = "\n"

    val size = file.length()

    private val channel: AsynchronousFileChannel

    init {
        if (!file.exists()) throw NoSuchFileException(file)

        channel = AsynchronousFileChannel.open(Paths.get(file.toURI()))
    }

    private var bufferContentLength = 0

    private val innerBuffer = ByteBuffer.allocate(capacity)

    private val sb = StringBuilder()

    private var finish = false

    private var position = 0L

    override suspend fun read(dst: ByteArray): Int {
        if (finish) throw IOException("read file finish")
        if (dst.size > innerBuffer.capacity() || dst.size > innerBuffer.limit() - innerBuffer.position()) {
            throw IndexOutOfBoundsException("dst size is larger than readable data size").apply { this.printStackTrace() }
        }

        while (bufferContentLength < dst.size) {
            val readLength = channel.aRead(innerBuffer, position)
            if (readLength > 0) {
                bufferContentLength += readLength
                position += readLength

            } else {
                finish = true
                break
            }
        }

        innerBuffer.flip()

        var length = innerBuffer.remaining()

        if (length > dst.size) length = dst.size

        innerBuffer.get(dst, 0, length)
        innerBuffer.compact()
        bufferContentLength -= length

        return length
    }

    suspend fun read(): Byte? {
        val byteArray = ByteArray(1)
        read(byteArray).let {
            return if (it == 0) null
            else byteArray[0]
        }
    }

    override suspend fun readLine(): String? {
        sb.delete(0, sb.length)

        val byteArray = ByteArray(1)

        while (true) {
            if (read(byteArray) == 0) {
                return null
            } else {
                val char = byteArray[0].toChar()

                sb.append(char)
                if (sb.endsWith(separator)) return sb.apply { this.delete(lastIndexOf(separator), this.length) }.toString()
            }
        }
    }

    @Deprecated("will always use stream mode", ReplaceWith("read"))
    suspend fun readBytes(): ByteArray {
        val byteArray = ByteArray(size.toInt())

        read(byteArray)
        return byteArray
    }

    override suspend fun write(byteArray: ByteArray, offset: Int, length: Int): Int {
        if (offset == 0 && length == byteArray.size) innerBuffer.put(byteArray)
        else innerBuffer.put(byteArray.copyOfRange(offset, offset + length))

        innerBuffer.flip()

        val written = channel.aWrite(innerBuffer, position)
        position += written
        innerBuffer.compact()
        return written
    }

    suspend fun write(byteArray: ByteArray): Int {
        return write(byteArray, 0, byteArray.size)
    }

    override suspend fun writeLine(string: String): Int {
        val byteArray = string.toByteArray()
        return write(byteArray, 0, byteArray.size)
    }

    override fun clear() {
        bufferContentLength = 0
        innerBuffer.clear()
    }

    fun reset() {
        position = 0L
        innerBuffer.clear()
        bufferContentLength = 0
        sb.delete(0, sb.length)
        finish = false
    }

    override fun close() = channel.close()
}
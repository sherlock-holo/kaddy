package kaddy.cache

import java.io.File
import java.util.concurrent.ConcurrentHashMap

object EtagCache {
    private val etagMap = ConcurrentHashMap<String, String>()

    fun addCache(etag: String, path: String) {
        etagMap[etag] = path
    }

    fun getCache(etag: String?): EtagStatus {
        if (etag == null) return EtagStatus.EMPTY

        if (etagMap[etag] == null) return EtagStatus.EMPTY

        return if (File(etagMap[etag]).lastModified().toString() == etag) EtagStatus.NEW
        else EtagStatus.OLD
    }

    fun updateCache(oldEtag: String, newEtag: String, path: String) {
        etagMap.remove(oldEtag)
        etagMap[newEtag] = path
    }

    enum class EtagStatus {
        NEW, OLD, EMPTY
    }
}
package kaddy.http

import kaddy.kio.ReadBuffer
import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.runBlocking
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousServerSocketChannel

fun main(args: Array<String>) = runBlocking {
    val server = AsynchronousServerSocketChannel.open()
    server.bind(InetSocketAddress(9876))

    val conn = server.aAccept()

    val readBuffer = ReadBuffer(conn).apply { this.separator = "\r\n" }
    val request = Request.buildRequest(readBuffer)
    println(request.version)
    println(request.host)
    println(request.uri)
    println(request.method)

    request.header.map.forEach {
        println("${it.key}: ${it.value}")
    }
}
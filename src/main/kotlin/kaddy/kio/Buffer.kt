package kaddy.kio

abstract class Buffer {
    abstract suspend fun read(dst: ByteArray): Int

    abstract suspend fun readLine(): String?

    abstract suspend fun write(byteArray: ByteArray, offset: Int, length: Int): Int

    abstract suspend fun writeLine(string: String): Int

    abstract fun clear()

    abstract fun close()
}
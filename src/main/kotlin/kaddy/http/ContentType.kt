package kaddy.http

@Deprecated("use Files.probeContentType")
val ContentType = HashMap<String, String>().apply {
    this["html"] = "text/html; charset=utf-8"
    this["htm"] = "text/html; charset=utf-8"
    this["css"] = "text/css"
    this["jpg"] = "image/jpeg"
    this["jpeg"] = "image/jpeg"
    this["gif"] = "image/gif"
    this["json"] = "application/json"
    this["js"] = "application/x-javascript"
    this["bmp"] = "application/x-bmp"
    this["cmx"] = "application/x-cmx"
    this["ico"] = "application/x-ico"
    this["img"] = "application/x-img"
    this["mp4"] = "video/mpeg4"
    this["mp3"] = "audio/mp3"
    this["txt"] = "text/plain"
}